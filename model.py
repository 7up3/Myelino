from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
import joblib
import numpy as np
import tensorflow as tf
import json


def preprocess_mri_image (image_path):
    # load model
    mri_model = load_model('mri_model2.h5')
    img = image.load_img(image_path, target_size=(224, 224)) 
    img = image.img_to_array(img) 
    img = np.expand_dims(img, axis=0) 
    # Make a prediction 
    prediction = mri_model.predict(img) 
    predicted_class_index = np.argmax(prediction) 
    class_labels = ["Negative", "Positive"] 
    result_mri = class_labels[predicted_class_index] 
    print("Prediction of image:", result_mri) 
    return result_mri
    

# Prediction function with saved model
def symptoms_predictions(input_data):
    try: 
        # # Load the saved model 
        gbc_model = joblib.load('gbc_model2.h5')
         
    except FileNotFoundError: 
        print("Error: Model file not found.") 
        return None 
    except Exception as e: 
        print("Error loading model:", e) 
        return None
     
        # process data
    input_data_as_numpy_array = np.asarray(input_data) 
    input_data_reshaped = input_data_as_numpy_array.reshape(1, -1) 
    # make prediction
    result = gbc_model.predict(input_data_reshaped)
    if result[0] == 1:
        print('symptoms: Positive')
        result_sym = "Positive"
    else:
        print(" symptoms:Negative")
        result_sym = "Negative"
    print(result_sym)
    return result_sym

# result of combination of models
def prediction_models(result_mri,result_sym):
    detection_label = None
    message= None
    if result_sym == "Positive" and result_mri == "Negative":
        detection_label = "negative"
        message = "This result depends only on your MRI image so we advise you to see a doctor."
    elif result_sym == "Negative" and result_mri == "Positive":
        detection_label = "positive"
        message = "This result is based on your MRI image, you should see a doctor."
    elif result_sym == "Negative" and result_mri == "Negative":
        detection_label = result_mri
        message = "You are fine."
    else:
        detection_label=result_mri
        message ="You should see a doctor."
    

    response = {"result": detection_label, "message": message}

    # print("Detection Label:", detection_label)
    # return detection_label
    return response
# Example usage:
# symptoms
# input_data = (1, 1, 2.0, 0, 1, 0, 1, 0) # negative
# input_data = (1,1,3.0,1,0,0,0,0) # positive ?
# pred_sym = symptoms_predictions(input_data)
# print("Prediction:", prediction)

# mri
# path = 'D:/API/negative.png' 
# path= 'D:/API/photo_2024-06-08_11-19-53.jpg'
# preprocess_mri_image(path)
# pred_mri = preprocess_mri_image(path)

# result of both
# prediction_models(pred_mri,pred_sym)