to install package python:
*******************************
pip install joblib==1.4.2 scikit-learn==1.2.2 tensorflow==2.15.0 Flask=1.1.2 marshmallow==3.21.3 numpy==1.25.2 keras==2.15.0
*******************************