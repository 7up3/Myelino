from flask import Flask, request, jsonify,Response
from werkzeug.utils import secure_filename
import os
import json
# for save the name of photo in hash code
from uuid import uuid1
# for process the symptoms arguments
from marshmallow import Schema, fields, ValidationError, validates , validates_schema
# handle requests code
import json
# tempfile library for save the image in pyhton local storage in production 
import tempfile
# import the libraries from model 
from model import preprocess_mri_image,symptoms_predictions,prediction_models


app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

app.secret_key = "secret key"
app.config['UPLOAD_FOLDER'] = tempfile.gettempdir()
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024

ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
info ="test done success"

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS
    
# Custom schema with validation for symptoms
class InputSchema(Schema):

    for i in range(1, 9): 
        locals()[f'arg{i}'] = fields.String(required=True)

    @validates_schema
    def validate_all_fields(self, data, **kwargs):
        for key, value in data.items():
            if not value.isdigit():
                raise ValidationError(f'{key} must be a non-empty numeric value')

@app.route('/detect', methods=['POST','GET'])
def lung_upload():
 
    if request.method == 'POST':
        # check if image is defined in request.files
        if 'file' not in request.files:
            return Response(
               response = json.dumps(
                   {"message":"No image part"}),
                   status = 500,
                   mimetype = 'application/json'
            )
        # check if the arguments is defined in request.form
        if not request.form:
            return Response(
                response = json.dumps({"message": "No input data provided"}),
                status = 400,
                mimetype = 'application/json'
                )
        # define schema that hold arguments of symptoms
        schema = InputSchema()
        image = request.files['file']

        if image.filename == '':
           return Response(
               response = json.dumps(
                   {"message":"No image selected for uploading"}),
                   status = 500,
                   mimetype = 'application/json'
            )
        if image and allowed_file(image.filename): 
            # save image
            image_name = str(uuid1()) + image.filename # the photo new name 3245_photoname.jpg for example
            filename = secure_filename(image_name)
            image_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
            os.makedirs(app.config['UPLOAD_FOLDER'], exist_ok=True)  # Ensure the folder exists
            image.save(image_path)
            try:
                # load arguments
                data = schema.load(request.form)
            except ValidationError as err:
                return jsonify(err.messages), 422
            # put symptoms int tuple as input for model
            args = tuple(int(value) for value in data.values()) 
            try:
                # # predict the image (the model) to predict the index (int)
                res_mri = preprocess_mri_image(image_path)
                res_sym = symptoms_predictions(args)
                # Remove the image after processing
                try:
                    os.remove(image_path)
                except Exception as e:
                    app.logger.error('Error removing file: %s', e)
                    return Response(
                        response=json.dumps({"message": f"Error removing file: {str(e)}"}),
                        status=500,
                        mimetype='application/json'
                    )
                # get the merged model
                return Response(
                   response = json.dumps(
                   prediction_models(res_mri,res_sym)),
                   status = 200,
                   mimetype = 'application/json'
                   )
            except Exception as e :
                print('*'*20)
                print(e)
                print('*'*20)
                return Response(
                   response = json.dumps(
                       {"message":"error - image classification"}),
                   status = 500,
                   mimetype = 'application/json'
                )
            
            
        else:
            return Response(
               response = json.dumps(
                   {"message":"Allowed image types are -> png, jpg, jpeg, gif"}),
               status = 500,
               mimetype = 'application/json'
            )
    elif request.method == 'GET':
        return Response(
            response= json.dumps({
                "result": info
                }))

# for test purposes 
@app.route('/test',methods=['GET'])
def test():
    return Response(
        response= json.dumps({
            'message':'You did it!!'
            }),
        status=200,
        mimetype='application/json')

@app.errorhandler(404)
def not_found(error):
    return Response(
       response = json.dumps(
           {"message":"route not found"}),
       status =500,
       mimetype = 'application/json'
    )

# to handle internal errors 
@app.errorhandler(500) 
def internal_error(error): 
    app.logger.error('Server Error: %s', error) 
    return Response( 
        response=json.dumps({"message": f"Internal server error : {error}"}),
        status=500, 
        mimetype='application/json' )
            
@app.errorhandler(502) 
def internal_error(error): 
    app.logger.error('Gateway Error: %s', error) 
    return Response( 
        response=json.dumps({"message": f"Bad Gateway : {error}"}),
        status=502, 
        mimetype='application/json' )

if __name__ == "__main__":
    app.run(port=5000,debug= True)


# see more at
# https://stackoverflow.com/questions/65298241/what-does-this-tensorflow-message-mean-any-side-effect-was-the-installation-su